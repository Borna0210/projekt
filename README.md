## Upute za aplikaciju prilagođene za Ubuntu
- Aplikacija će koristiti 2 terminala, 
  na jednom će cijelo vrijeme biti pokrenut program projektR.py koji pasivno i aktivno skenira mrežu,
  a na drugom će biti pokrenut flask server

## Instaliranje svih potrebnih paketa i postavljanje baze podataka

- Kako bi instalirali sve potrebne pakete moramo u folderu FlaskApp pokrenuti dvije skripte,
  prvo pokrećemo skriptu install.sh, a nakon što ona napravi sve potrebno, pokrećemo skriptu createdb.sh
 
- **$ chmod +x install.sh** 
- **$ ./install.sh**  
- **$ chmod +x createdb.sh**  
- **$ ./createdb.sh**  

## Pokretanje programa

- ### Prije pokretanja programa, napišite interface koji želite skenirati u konfiguracijsku datoteku interface.txt, sve interfaceove možete vidjeti naredbom **$ ip link show** 

- ### Također, prije pokretanja programa upišite path foldera FlaskApp u varijablu path na početku datoteke projektR.py, to smo morali implementirati jer servis nije htio raditi s os.getcwd()


### Pokretanje programa kao servis

- Kako bi pokrenuli program kao servis, prvo trebate premjestiti datoteku projektR.service u
  **/etc/systemd/system**
- Sljedeće naredbe za pokretanje su:
- **sudo systemctl daemon-reload**
- **sudo systemctl enable projektR.service**
- **sudo systemctl start projektR.service**

- Za provjeru statusa servisa:
- **sudo systemctl status projektR.service**

- Za prekid servisa:
- **sudo systemctl stop projektR.service**

- Za gašenje servisa:
- **sudo systemctl disable projektR.service**


### Pokretanje programa općenito u terminalu

- Kako bi pokrenuli program, prvo moramo otvoriti dva terminala u folderu FlaskApp
- U jednom od dva terminala moramo ući u root korisnika --> **$ sudo su**
- U tom terminalu možemo odmah pokrenuti projektR koji skenira mrežu --> **# python3 projektR.py**


## Pokretanje virtualnog okruženja

- Pokrećemo virtualno okruženje iz FlaskApp foldera  
- **$ source venv/bin/activate**
- Pokrećemo program s naredbom --> **$ flask run**
- Pisat će adresa na kojoj se može pristupiti stranici, ali vjerojatno će ona biti 127.0.0.1:5000
- Kada ste na stranici, možete vidjeti trenutnu bazu ili pokrenuti aktivni sken
- U polje možete upisati npr. 192.168.1.5 ako želite aktivni sken jedne adrese ili 192.168.1.5/24 ako želite aktivni sken raspona IP-adresa

