from flask import Flask, render_template, request,redirect
from flask_mysqldb import MySQL
import MySQLdb.cursors
import re
import os
from time import sleep

app = Flask(__name__)

#app.secret_key = 'SuperUser9275'

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'admin'
app.config['MYSQL_PASSWORD'] = 'SuperUser9275'
app.config['MYSQL_DB'] = 'projektR'

mysql = MySQL(app)
ip_address=0
@app.route("/")
@app.route("/home")
def home():
	ipExists = fileExists()
	return render_template('index.html', ipExists = ipExists)

@app.route("/search", methods = ['GET', 'POST'])
def search():
	ip_address = request.form['ipinput']
	regex_ip_pattern = "^([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])((/([1-9]|[12][0-9]|3[0-2]))?)$"
	if request.method == 'POST' and bool(re.match(regex_ip_pattern, ip_address)):
		f= open("ip.txt","w+")
		f.write(ip_address)
		f.close()
		lista = []
		if '/' in ip_address:
			sleep(1)
			return redirect("/db")
		else:
			sleep(1)
			return redirect("/ip")	
	else:
		msg = ''
		msg = 'Unijeli ste netočan format IP adrese'
		return render_template('index.html', msg = msg, ipExists = ipExists)

#Pretrazuje trenutni direktorij kako bi saznao da li je scan aktivan
def fileExists():
	ip = "ip.txt"
	files = []
	for subdir, dirs, files in os.walk('./'):
		for file in files:
			if (file == ip):
				return 1
	
	return 0


@app.route("/db", methods = ['GET', 'POST'])
def db():
	lista = []
	ipExists = fileExists()
	cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
	cursor.execute('SELECT * from netinfo order by zadnja_pojava desc')
	for row in cursor:
		lista.append(row)
	cursor.close()
	return render_template('table.html', lista=lista, ipExists = ipExists)

@app.route("/ip", methods = ['GET', 'POST'])
def ip():
	global ip_address
	if(os.path.isfile("ip.txt")):
		f=open("ip.txt","r+")
		ip_address = f.readlines()
		f.close()
	lista = []
	ipExists = fileExists()
	cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
	cursor.execute('SELECT * from netinfo where ip=% s order by zadnja_pojava desc limit 1',(ip_address))
	for row in cursor:
		lista.append(row)
	cursor.close()
	return render_template('table.html', lista=lista, ipExists = ipExists)



@app.route("/edit", methods = ['GET', 'POST'])
def edit():
	#params = request.form.to_dict()
	ipExists = fileExists()
	params = request.form['mac']
	return render_template('details.html', params=params, ipExists = ipExists)

@app.route("/save", methods = ['GET', 'POST'])
def save():
	komentar = request.form['komentar']
	mac = request.form['mac']
	cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
	cursor.execute('UPDATE netinfo SET komentar= % s WHERE mac = % s', (komentar, mac, ))
	mysql.connection.commit()
	return redirect("/db")
	#lista = []
	#cursor.execute('SELECT * from netinfo order by zadnja_pojava desc')
	#for row in cursor:
	#	lista.append(row)
	#cursor.close()
	#return render_template('table.html', lista=lista)	
