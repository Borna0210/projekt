from scapy.all import *
from datetime import datetime
import nmap
from threading import Thread
import mysql.connector
from mysql.connector import Error
from getmac import get_mac_address
import os

arp_info = {}  # Riječnik u koji ce se spremati parovi IP : MAC adresa na mreži
ip_list = []  # Lista u koju će se spremati IP adrese kojima je u datom trenutku pridružena neka MAC adresa
current = {}  # Riječnik u koji će se spremati parovi MAC adresa i ažuriranja trenutnog vremena njihovog pojavljivanja u mreži
first = {}  # Riječnik u koji će se spremati parovi MAC adresa i točnog vremena njihovog prvog pojavljivanja u mreži


path="/home/petar//ProjektR/projekt/FlaskApp" # Direktorij u kojem se nalazi ovaj program


with open (path+"/interface.txt", "r") as myfile:
    interface=myfile.readline().strip() # Sucelje koje pregledavamo
   


def arp_sniff():  # Funkcija koja radi arp sniff cijelo vrijeme dok korisnik to ne prekine
    return sniff(iface=interface,filter="arp", store=0, prn=arp_collect)


def arp_collect(arp_pkg):  # Funkcija koju arp_sniff cijelo vrijeme izvršava, ona reagira na dolazak svakog arp paketa u mrežu
    mac = arp_pkg[ARP].hwsrc  # MAC adresa uređaja u mreži
    # IP adresa na koju se taj uređaj spojio u lokalnoj mreži
    ip = arp_pkg[ARP].psrc
    # Prethodno pridruživanje nekog uredaja s odabranom IP adresom
    current_link=get_key_from_value(arp_info, ip)
   
    

    # Ako nema uređaja u mreži, spaja se i zauzima IP adresu u mreži
    if arp_info.get(mac) == None:
        # Ako je IP bio prije pridružen nekoj drugoj MAC adresi, to se popravlja u rjecnicima arp_info, first i current
        if(current_link!=None):
            
            ip_list.remove(ip)
            del arp_info[current_link]
            del first[current_link]
            del current[current_link]
            
          
        
        arp_info[mac] = ip
        ip_list.append(ip)

        temp = datetime.now()
        temp = temp.strftime("%d-%m-%Y %H:%M:%S")
        first[mac] = temp
        current[mac] = temp

        
        # Zapis u bazu podataka, zapisuju se MAC adresa i IP adresa kojoj je pridružena,
        # prvo vrijeme pridruživanja MAC i IP i trenutno vrijeme koje se osvježava ako je MAC adresa i dalje pridružena istoj IP adresi
        
        insertFour(mac, ip, first.get(mac), current.get(mac))
        

    # Ako uređaj odjednom promjeni IP adresu u mreži, ovako se program može od toga oporaviti i pvoezati MAC adresu uređaja s tom IP adresom
    elif ip != arp_info[mac]:
        ip_list.remove(arp_info[mac])
        arp_info[mac] = ip
        ip_list.append(ip)

        temp = datetime.now()  # Datum prvog čitanja pridruživanja MAC i IP adrese
        temp = temp.strftime("%d-%m-%Y %H:%M:%S")
        first[mac] = temp
        current[mac] = temp

        # Zapis u bazu podataka, zapisuju se MAC adresa i IP adresa kojoj je pridružena,
        # prvo vrijeme pridruživanja MAC i IP i trenutno vrijeme koje se osvježava ako je MAC adresa i dalje pridružena istoj IP adresi
        
        insertFour(mac, ip, first.get(mac), current.get(mac))
    
    # Periodički ispis svih uređaja u mreži u terminal, kontrolni ispis
    print(ip_list)

    # Provjeravamo za svaki uređaj koji je pri prošlom ispitivanju bio u mreži je li i dalje u mreži
    for ip in ip_list:
        # Provjeravamo je li IP aktivan
        HOST_UP = True if os.system("fping --quiet -r 5 " + ip) == 0 else False

        # Ako je IP aktivan, dobijemo MAC adresu preko IP adrese i osvježimo zadnju pojavu uređaja u mreži
        if(HOST_UP):
            macc = list(arp_info.keys())[list(arp_info.values()).index(ip)]
            temp = datetime.now()
            temp = temp.strftime("%d-%m-%Y %H:%M:%S")
            current[macc] = temp
            # Upisuje zadnju pojavu MAC adrese vezane uz IP u bazu ako je i dalje aktivan par MAC:IP
            if(get_mac_address(ip=ip)==macc or get_mac_address(ip=ip)==None):
                updateFourth(current[macc], macc, ip, first[macc])
            
        
            
# Funkcija koja obavlja aktivno skeniranje, nju poziva funkcija sweep malo kasnije

def scanner(nm, val, scandate):  # Prima tri parametra, nmap koji skenira portove, IP adresa ili opseg njih koji gledamo i vrijeme skeniranja
    portinfo_list = []  # Lista informacija o portovima
    print("START")  # Početak aktivnog skeniranja

    # U ovom try-catch bloku provjeravamo može li se prepoznati operacijski sustav uređaja,
    # a neki uređaji s npr. većom razinom zaštite i promjenjivom vanjskom MAC adresom mogu nekada ne dopuštati dijeljenje podataka pa i to može baciti iznimku
    try:
        HOST_UP2 = True if os.system(
            "fping --quiet -r 5 " + val) == 0 else False  # Provjeravamo je li IP adresa aktivna i ako je, pokrećemo sken
        if(HOST_UP2):
            # Pokrećemo portscan koji provjerava može li se saznati operacijski sustav uređaja, limitira provjeru na one koji pokazuju znakove toga da bi mogli imati OS koji se može detektirati,
            # A ako ga ne može detektirati sa sigurnošću, može ga pokušati pogoditi i vratiti taj pogodak i podatak u obliku postotka koliko je siguran u njega
            machine = nm.scan(
                val, arguments='-O --osscan-limit --osscan-guess -v -sV --open -T5')

            # Određivanje svih informacija vezanih uz OS uređaja
            machineOS = machine['scan'][val]['osmatch'][0]['osclass'][0]['osfamily']+' ' + \
                machine['scan'][val]['osmatch'][0]['osclass'][0]['osgen'] + \
                ' ('+machine['scan'][val]['osmatch'][0]['osclass'][0]['accuracy']+'%)'
        else:
            # Ako HOST_UP2 vrati False, IP nije u mreži pa vraćamo to
            print("Ne postoji uređaj spojen na IP u mreži")

    except Exception:
        # Ako dobijemo iznimklu na nm.scan-u, onda je moguće ne možemo prepoznati OS ili da uređaj trenutno ne dopušta dijeljenje podataka o njemu
        machineOS = "Operacijski sustav nije moguce prepoznati/Uredaj trenutno ne dopusta dijeljenje podataka"

    try:
        open_ports = 0  # Kontrolna varijabla koja će provjeravati ima li otvorenih portova
        if(HOST_UP2):
            # Provjeravamo ima li otvorenih portova
            try:
                skenirani_ip = machine['scan'][val]
                open_ports = 1
            except:
                portinfo_list.append(
                    "Nema otvorenih portova/Uredaj trenutno ne dopusta dijeljenje podataka")

            # Ako ima otvorenih portova, skenirati ćemo ih
            if(open_ports):
                # Svi protokoli koji su pronađeni (tcp, udp)
                protokoli = skenirani_ip.all_protocols()
                print(protokoli)  # Ispis protokola, kontrolna varijabla

                # Za svaki protokol pokrećemo dublji sken
                for protokol in protokoli:
                    # trenutni protokol koji obrađujemo
                    skenirani_ip_protokol = skenirani_ip[protokol]
                    # lista u koju spremamo portove koji koriste odabrani protokol (npr. 53, 80...)
                    portlist = list(skenirani_ip_protokol.keys())

                    # Za svaki port u listi provjeravamo podatke o imenu i verziji protokola
                    for port in portlist:
                        # Tri varijable u koje ćemo unositi podatke o određenom portu
                        name = ''
                        version = ''
                        extrainfo = ''
                        print(port)  # Ispis porta, kontrolna varijabla

                        # Dohvaćanje imena određenog porta i ispis za kontrolu
                        portname = skenirani_ip_protokol[port]['name']
                        print(portname)
                        protocol_version = ''  # String u koji ćemo ubaciti sve informacije o verziji protokola

                        # Provjeravamo ima li port određene atribute vezane uz verziju i ubacujemo ih u protocol_version
                        if(skenirani_ip_protokol[port]['product']):
                            name = skenirani_ip_protokol[port]['product']
                            protocol_version += name
                            # Ako u verziji porta postoji zapis "Microsoft Windows", znamo da je to OS uredaja,
                            # pa to mozemo upisati u varijablu jer nmap ne prepoznaje MS Windows OS
                            if("Microsoft Windows" in name):
                                machineOS="Microsoft Windows (100%)"
                        if(skenirani_ip_protokol[port]['version']):
                            version = skenirani_ip_protokol[port]['version']
                            protocol_version += ' '+version

                        if(skenirani_ip_protokol[port]['extrainfo']):
                            extrainfo = '(' + \
                                skenirani_ip_protokol[port]['extrainfo']+')'
                            protocol_version += ' '+extrainfo

                        if(protocol_version == ''):
                            protocol_version = "No version found"

                        # Sve informacije o portu spremamo u string portinfo
                        portinfo = "PORT: " + \
                            str(port)+'/'+protokol+',  '+"NAME: " + \
                            portname+',  '+"VERSION: "+protocol_version

                        # Na kraju portinfo dodajemo u listu koja sadrži portinfo o svim portovima nekog uređaja
                        portinfo_list.append(portinfo)

    # Ako uhvatimo iznimku, moguće je da nema otvorenih portova ili da uređaj ne dopušta dijeljenje podataka u ovom trenutku
    except Exception:
        portinfo_list.append(
            "Nema otvorenih portova/Uređaj trenutno ne dopušta dijeljenje podataka")

    # Na kraju gledamo je li uređaj u mreži i ako je, zapisujemo sve informacije koje smo dobili
    # U međuvremenu je moguće da uređaj više nije u mreži zbog trajanja nmap skena pa se možemo osigurati s HOST_UP3
    HOST_UP3 = True if os.system(
        "fping --quiet -r 5 " + val) == 0 else False
    if(HOST_UP3):
        # Listu portinfo_list pretvaramo u string i vrijednosti odvajamo s ||
        portinfo_str = ' || '.join(portinfo_list)

        # Isto kao s OS, provjeravamo postoji li MAC adresa spojena na IP koji provjeravamo, ako postoji,
        # dodajemo sve podatke na točno mjesto gdje trebaju ići
        try:
            macc2 = list(arp_info.keys())[list(arp_info.values()).index(val)]

            #Dodajemo zadnja 3 zapisa, OS, port info i datum zadnjeg skeniranja
            updateLastThree(machineOS, portinfo_str, scandate,
                            macc2, val, first[macc2])
        except:
            print(val+" nije pronađen u mreži")

# Funkcija koja provjerava je li pozvano aktivno pretraživanje
def check_sweep():
    while True:
        if(os.path.isfile(path+"/ip.txt")):
            ipsearch=''
            while(len(ipsearch)==0):
                with open(path+"/ip.txt") as f:
                    for l in f:
                        ipsearch=l
                if(len(ipsearch)!=0):
                    sweep(ipsearch)
                    os.remove(path+"/ip.txt")



# Funkcija koja prije početka aktivnog pretraživanja upiše sve trenutno pridružene IP-eve i osnovne podatke o njima u bazu
def before_sweep(nm, n, val):
    if(n == 1):
        check_if_str=isinstance(val,str)
        if(check_if_str):
            HOST_UP2 = True if os.system(
                "fping --quiet -r 5 " + val) == 0 else False  # Provjeravamo je li IP adresa aktivna i ako je, pokrećemo sken
        else:
            HOST_UP2 = True if os.system(
                "fping --quiet -r 5 " + val[0]) == 0 else False  # Provjeravamo je li IP adresa aktivna i ako je, pokrećemo sken
        if(HOST_UP2):
            if(check_if_str):
                # Prije skena napravimo sigurnosni ping scan kako bi nmap prepoznao IP i stavio ga u listu IP adresa, brzo je gotovo, a pomogne ako se uređaji ne otkrivaju brzo
                for i in range(10):
                    safety = nm.scan(val, arguments='-sP')
            else:
                for i in range(10):
                    safety = nm.scan(val[0], arguments='-sP')
    elif(n > 1):
        for i in range(n):
            # Provjeravamo je li IP adresa aktivna i ako je, pokrećemo sken
            HOST_UP2 = True if os.system(
                "fping --quiet -r 5 " + val[i]) == 0 else False
            if(HOST_UP2):
                # Prije skena napravimo sigurnosni ping scan kako bi nmap prepoznao IP i stavio ga u listu IP adresa, brzo je gotovo, a pomogne ako se uređaji ne otkrivaju brzo
                for j in range(10):
                    safety = nm.scan(val[i], arguments='-sP')

# Funkcija koja pokreće aktivan sken
def sweep(val):
    print("SWEEP")
    scandate = datetime.now()  # Vrijeme skeniranja
    scandate = scandate.strftime("%d-%m-%Y %H:%M:%S")
    nm = nmap.PortScanner()  # Funkcija koja obavlja skeniranje portova
    if('/' not in val):  # Gledamo je li IP adresa pojedinačna
        before_sweep(nm, 1, val)
        scanner(nm, val, scandate)  # Pozivamo funkciju scanner
        print("SWEEP DONE")

    elif('/' in val):  # Ako nije pojedinačna, provjeravamo je li ona opseg određen maskom
        response = os.popen(f"fping -aq -r 2 -g {val}").read()
        # Čitamo sve aktivne IP adrese u zadanom rasponu i zapisujemo ih u listu za kasniju iteraciju
        lista = list(filter(None, response.split("\n")))
        print("Aktivni IP-evi")
        print(lista)  # Ispis aktivnih IP adresa, kontrolna varijabla
        before_sweep(nm, len(lista), lista)
        for val in lista:
            # Pozivamo funkciju scanner za svaki aktivni IP u rasponu
            scanner(nm, val, scandate)
        print("SWEEP DONE")


# Funkcije za zapis u bazu podataka
def insertFour(mac, ip, prva_pojava, zadnja_pojava):
    cursor = connection.cursor()
    cursor.execute('INSERT INTO netinfo VALUES (%s, %s, %s, %s, NULL, NULL, NULL, NULL)',
                   (mac, ip, prva_pojava, zadnja_pojava))
    # print("DONE")
    connection.commit()


def updateFourth(zadnja_pojava, mac, ip, prva_pojava):
    cursor = connection.cursor()
    cursor.execute('UPDATE netinfo SET zadnja_pojava= %s WHERE mac = %s AND ip = %s AND prva_pojava = %s',
                   (zadnja_pojava, mac, ip, prva_pojava))
    # print("UPDATED")
    connection.commit()


def updateLastThree(os, portinfo, zadnji_scan, mac, ip, prva_pojava):
    cursor = connection.cursor()
    cursor.execute('UPDATE netinfo SET OS= %s, portinfo= %s, zadnji_scan= %s WHERE mac = %s AND ip = %s AND prva_pojava = %s',
                   (os, portinfo, zadnji_scan, mac, ip, prva_pojava))
    connection.commit()

# Ako postoji datoteka u kojoj prenosimo IP na samom početku programa, brišemo ju
def if_ip_at_start():
    if(os.path.isfile(path+"/ip.txt")):
        os.remove(path+"/ip.txt")
       
# Provjeravamo kljuc rjecnika iz njegove vrijednosti, korisno za saznati MAC adresu od pridružene IP adrese
def get_key_from_value(d, val):
    keys = [k for k, v in d.items() if v == val]
    if keys:
        return keys[0]
    return None

# Ako se main pokrene eksplicitno nad ovom datotekom, pokrenut će se dvije dretve, jedna će pasivno provjeravati ARP pakete koji dolaze u mrežu kad se uređaji spajaju
# Druga će čekati signal za aktivno pretraživanje mreže
if __name__ == '__main__':
    if_ip_at_start()

    # Povezivanje s bazom podataka
    try:
        connection = mysql.connector.connect(host='localhost',
                                             database='projektR',
                                             user='admin',
                                             password='SuperUser9275')
        if connection.is_connected():
            db_Info = connection.get_server_info()
            print("Connected to MySQL Server version ", db_Info)
            cursor = connection.cursor()
            cursor.execute("select database();")
            record = cursor.fetchone()
            print("You're connected to database: ", record)
    except Error as e:
        print("Error while connecting to MySQL", e)
    
    # Pokretanje pasivnog traženja i provjere je li aktivno pokrenuto
    Thread(target=arp_sniff).start()
    Thread(target=check_sweep).start()
