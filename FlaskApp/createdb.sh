#!/bin/bash
sudo mysql -u root -p << EOF
CREATE DATABASE IF NOT EXISTS projektR;
CREATE USER IF NOT EXISTS 'admin'@'localhost' IDENTIFIED BY 'SuperUser9275';
GRANT ALL PRIVILEGES ON * . * TO 'admin'@'localhost';
USE projektR;
CREATE table IF NOT EXISTS netinfo(mac VARCHAR(50) NOT NULL, ip VARCHAR(50) NOT NULL,
prva_pojava VARCHAR(255) NOT NULL, zadnja_pojava VARCHAR(255) NOT NULL, zadnji_scan VARCHAR(255), OS VARCHAR(255), portinfo TEXT, komentar TEXT, PRIMARY KEY(mac,ip,prva_pojava));
EOF
