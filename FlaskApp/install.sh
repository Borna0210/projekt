#!/bin/bash
sudo apt install python3-pip
sudo apt install fping
sudo pip3 install scapy
sudo pip uninstall nmap
sudo pip uninstall python-nmap
sudo pip3 install python-nmap
sudo apt install nmap
sudo pip3 install getmac
sudo pip3 install mysql-connector-python
sudo apt install --reinstall mysql-server-8.0
sudo apt install libmysqlclient-dev
sudo apt install python3-flask
sudo pip3 install flask-mysqldb
sudo apt install python3.8-venv
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
